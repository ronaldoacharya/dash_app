from datetime import timedelta
import random
import pandas as pd
from config import colors_list

def process_data():
    df = pd.read_csv('source.csv')
    # df=pd.read_excel('source.xlsx')
    df['Start'] = pd.to_datetime(df["Start"])
    df['End'] = pd.to_datetime(df["End"])
    df['Deadline'] = pd.to_datetime(df["Deadline"])
    try:
        df['Duration'] = pd.to_datetime(df["Duration"])
    except:
        pass
    df['date'] = df['Start'].dt.date
    df['base_time'] = df['date'].apply(lambda x: pd.to_datetime(str(x) + ' 12:00:00'))

    df["transparent_height"] = df['Start'] - df['base_time']
    df["transparent_height"] = df['transparent_height'].apply(lambda x: int(x.seconds/60))

    df["deadline_marker"] = df['Deadline'] - df['base_time']
    df["deadline_marker"] = df['deadline_marker'].apply(lambda x: int(x.seconds/60))

    df["deadline_height"] = df['End'] - df['Deadline']
    df["deadline_height"] = df['deadline_height'].apply(lambda x: 0 if x < timedelta(0) else x.seconds/60)

    df["warning_time"] = df['Deadline'].apply(lambda x: x-timedelta(hours=1))
    df["warning_height"] = df['End'] - df['warning_time']
    df["warning_height"] = df['warning_height'].apply(lambda x: 0 if x < timedelta(0) else x.seconds/60)
    df["warning_height"] = df['warning_height'] - df['deadline_height']

    df["user_height"] = df['warning_time'] - df['Start']
    df["user_height"] = df['user_height'].apply(lambda x: 0 if x < timedelta(0) else x.seconds/60)

    df["is_warning"] = df['warning_height'].apply(lambda x: x > 0)
    df["is_alert"] = df['deadline_height'].apply(lambda x: x > 0)
    # df["user_height"] = df['user_height'].apply(lambda x: 0 if x < timedelta(0) else x.seconds/60)
    users=list(df['Username'].unique())
    
    unique_colors=random.sample(colors_list, len(users))
    for index,user in enumerate(users):
        df.loc[df['Username'] ==user, 'color']=unique_colors[index]

    # converting duration to hours
    # df['duration_worked'] = df['Duration'].apply(lambda x: datetime.strptime(x[0:8], "%H:%M:%S"))
    df['duration_worked'] = df['Duration'].apply(lambda x:x.hour+x.minute/60+x.second/60)
    df=df.sort_values(by=['date','Username'], ascending=True)
    days=df['date'].unique()
    colors_of_users=dict(zip(df['Username'], df['color']))
    legend_data = [{'color': v, 'title': k} for k,v in colors_of_users.items()]
    return df,days,legend_data
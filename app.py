import plotly.graph_objects as go
import dash
from dash import dcc, html, dash_table, Input, Output,no_update
from data import process_data
from config import *

global data,days,legend_data
data,days,legend_data=process_data()

# Create Dash app
app = dash.Dash(__name__)

def get_yaxislabels(position,range_val):
    g = 12
    b = 13
    _d = []
    for i in range(range_val):
        if b == 24:
            b = 0
        if g == 24:
            g = 0
        if position=='left':
            _d.append(f'{"0" if b<10 else ""}{b}:00  {"0" if g<10 else ""}{g}:00')
        elif position=='right':
            _d.append(f'{"0" if g<10 else ""}{g}:00  {"0" if b<10 else ""}{b}:00')
        b+=1
        g+=1
    # _d.append("          ")
    if position=='left':
        _d.append(" BST  GMT ")
    elif position=='right':
        _d.append(" GMT  BST ")
    return _d


def make_chart(data,days):
    fig = go.Figure()

    for day_index,day in enumerate(days):
        day_start_pos=day_index*DAY_WIDTH
        #  draw margin for everyday
        fig.add_trace(
        go.Scatter(
                    x=[day_start_pos,day_start_pos],  # Set x coordinates for the vertical line
                    y=[0, 1500],  # Set y coordinates for the vertical line
                    mode='lines',
                    line=dict(color=DAY_MARGIN_COLOR,width=1),  # Set line color and width
                    # name='Vertical Line'
                    showlegend=False,
                    hoverinfo='none',
                ))
        df=data[data['date']==day]
        for i in range(len(df)):
            x_positions=[day_start_pos+i*BAR_WIDTH+BAR_MARGIN]
            user_data=df.iloc[i].to_dict()
            transparent_height=user_data['transparent_height']
            user_height=user_data['user_height']
            warning_height=user_data['warning_height']
            deadline_height=user_data['deadline_height']
            user_color=df['color'].iloc[i]
            fig.add_trace(go.Bar(
                x=x_positions,
                y=[transparent_height],
                marker_color='rgba(255, 255, 255, 0)',
                showlegend=False,
                hoverinfo='none',
            ))

            # # before warning 
            fig.add_trace(go.Bar(
                x=x_positions,
                y=[user_height],
                marker_color=user_color,
                marker_line_color='black',
                marker_line_width=BAR_PERIMETER if user_height!=0 else 0,
                hoverinfo='none',
                showlegend=False,
                customdata=[user_data],

            ))

            # warning 
            fig.add_trace(go.Bar(
                x=x_positions,
                y=[warning_height],
                marker_color="yellow" if  user_data['Start'] < user_data['Deadline'] else 'rgba(0, 0, 0, 0)',
                marker_line_color='black' if  user_data['Start'] < user_data['Deadline'] else 'rgba(0, 0, 0, 0)',
                marker_line_width=BAR_PERIMETER if  user_data['Start'] < user_data['Deadline'] else 0,
                showlegend=False,
                hoverinfo='none',
                customdata=[user_data],

            ))

            # alert or deadline
            fig.add_trace(go.Bar(
                x=x_positions,
                y=[deadline_height],
                marker_color="red",
                marker_line_color='black',
                marker_line_width=BAR_PERIMETER if deadline_height!=0 else 0,
                showlegend=False,
                hoverinfo='none',
                customdata=[user_data],

            ))
            #deadline marker
            fig.add_trace(
                go.Scatter(
                x=[x_positions[0]-BAR_WIDTH/3,x_positions[0]+BAR_WIDTH/3],
                y=[user_data['deadline_marker'],user_data['deadline_marker']],
                mode='lines',
                line_width=DEADLINE_MARKER_WIDTH,
                marker=dict(color='red'),
                showlegend=False,
                hoverinfo='none',
                customdata=[user_data],
            )
                )
    # This is for margin of last day
    fig.add_trace(go.Scatter(
                x=[day_start_pos+DAY_WIDTH,day_start_pos+DAY_WIDTH],  # Set x coordinates for the vertical line
                y=[0, 25*60],  # Set y coordinates for the vertical line
                mode='lines',
                line=dict(color=DAY_MARGIN_COLOR,width=1),  # Set line color and width
                # name='Vertical Line'
                hoverinfo='none',
                showlegend=False,
                yaxis="y2"
            ))

    fig.update_layout(
        title=f"Data from {days[0]} to {days[-1]}",  # Add the title
        title_font=dict(size=18, family='Arial, sans-serif', color='black'),  # Adjust title font properties
        title_x=0.5,  # Center the title horizontally
        title_y=0.95,
        xaxis=dict(
            showline=True,
            # linewidth=5,
            tickmode='array',
            tickvals= [DAY_WIDTH*i+DAY_WIDTH/2 for i in range(len(days))] ,
            ticktext= days,
            tickfont=dict(size=15),
        ),
        
        yaxis=dict(
            # title='Duration (hours)',
            side='left',
            tickvals= [i*60 for i in range(26)] ,
            ticktext= get_yaxislabels('left',25),
            tickfont=dict(size=14),
            range=[0,25*60]
            # position=0
            ),
        yaxis2=dict(
            overlaying='y',
            side='right',
            tickvals= [i*60 for i in range(26)] ,
            ticktext= get_yaxislabels('right',25),
            tickfont=dict(size=14),
            position=1,
            range=[0, 25*60],
        ),
        barmode='stack',
        # hovertemplate=f"<b>User:</b>",
        bargap=BAR_GAP,
        height=GRAPH_HEIGHT,
        width=GRAPH_WIDTH,
        shapes=[
            {'type': 'line', 
        'x0': 0, 
        'x1': len(days)*DAY_WIDTH, 
        'y0': (i*30), 'y1': (i*30),
         'line': {'color': 'black', 'width': WIDTH_YMINOR}} 
         for i in range(1,49,2)]+
         [
            {'type': 'line', 
        'x0': 0, 
        'x1': len(days)*DAY_WIDTH, 
        'y0': (i*60), 'y1': (i*60),
         'line': {'color': 'black', 'width': WIDTH_YMAJOR}} 
         for i in range(25)],         
        # title='User Activity Duration',
    )
    # adding_arrow at x and y-axis
    fig.add_annotation(
    x=0,
    y=60*25,
    ax=0,
    ay=0,
    xref='x',
    yref='y',
    axref='x',
    ayref='y',
    text='',  
    showarrow=True,
    arrowhead=1,
    arrowsize=2,
    arrowwidth=1,
    arrowcolor='black'
    )
    fig.add_annotation(
    x=len(days)*DAY_WIDTH,
    y=60*25,
    ax=len(days)*DAY_WIDTH,
    ay=0,
    xref='x',
    yref='y',
    axref='x',
    ayref='y',
    text='',  
    showarrow=True,
    arrowhead=1,
    arrowsize=2,
    arrowwidth=1,
    arrowcolor='black'
    )
    fig.add_annotation(
    x=len(days)*DAY_WIDTH+0.1,
    y=0,
    ax=0,
    ay=0,
    xref='x',
    yref='y',
    axref='x',
    ayref='y',
    text='',  
    showarrow=True,
    arrowhead=3,
    arrowsize=2,
    arrowwidth=1,
    arrowcolor='black'
    )
    return fig

def make_minichart(hoverdata):
    mini_chart = go.Figure()
    duration_df=data[data['Username']==hoverdata['Username']]
    durations=duration_df['duration_worked'].to_list()
    dates=duration_df['date'].to_list()
    mini_chart.add_trace(go.Bar(
        x=dates,
        y=durations,
        marker_color='black',
        # title="My Bar Chart"
    ))
    # Update layout with title and styling

    mini_chart.update_layout(
        height=250,
        plot_bgcolor=hoverdata['color'],  # Background color for the plot area
        paper_bgcolor=hoverdata['color'], # Background color for the entire chart
        yaxis=dict(title="Duration in hours",color='black',showline=True),
        xaxis=dict(title="Days",color='black',showline=True)
    )
    return mini_chart

def get_mainapp_container(data,days,legend_data):
    children=[
    dcc.Graph(
                id='user-activity-bar-chart',
                clear_on_unhover=True,
                style={'margin-top': '-20px'},
                # figure=make_chart(data,days)
                # figure=fig
            ),
    html.Div("KEYS",style={'font-size':'20px','padding-left':'20px'}),
    html.Div(children=[
        html.Div([
            html.Div(
                style={'background-color': item['color'],
                    'width': '20px', 'height': '20px',
                    'display': 'inline-block'},
            ),
            html.Span(item['title'], style={'padding-left': '10px'})
        ],style={'padding':'30px'}) for item in legend_data
    ],style={'display':'flex'})
    ]
    return children

# APP Component starts here
app.layout = html.Div([
    dcc.Location(id='url', refresh=True),
    dcc.RadioItems(id="filter",
    options=[
            {'label': 'All', 'value': '1'},
            {'label': 'Warnings and alert', 'value': '2'},
            {'label': 'Alerts only', 'value': '3'}
        ],
        # default
    value='1',
    style={'display': 'flex',"font-size": "20px",'text-align': 'center'},
    inputStyle={
            'margin-right': '5px',
            'transform': 'scale(2)',  # Increase button size
        },
    labelStyle={
        'display': 'block',
        'text-align': 'center',  # Align text to center
        'padding': '30px' #spacing
    }),
    html.Div(id='mainapp',children=get_mainapp_container(data,days,legend_data)),
    dcc.Tooltip(id="graph-tooltip",loading_text='Loading',children=[]),
    # html.Div(id="filter-out", children=[
    #     dash_table.DataTable(data=data.to_dict('records'), page_size=100)
    # ]),
      
])


@app.callback(
    Output(component_id='user-activity-bar-chart', component_property='figure'),
    Input(component_id='filter', component_property='value'),
)
def change_filter(input_value):
    _data = data
    if input_value == "2":
        _data=_data[_data['is_warning']==True]
    elif input_value == "3":
        _data=_data[_data['is_alert']==True]
    return make_chart(_data,days)

@app.callback(
    Output("graph-tooltip", "show"),
    Output("graph-tooltip", "bbox"),
    Output('graph-tooltip', 'style'),
    Output("graph-tooltip", "children"),
    Input("user-activity-bar-chart", "hoverData"),
)
def display_hover(hoverData):
    if hoverData is None:
        return False, no_update,no_update, no_update
    pt = hoverData["points"][0]
    bbox = pt["bbox"]
    # to adjust hovering in the screen
    if bbox['y0']<250:
        bbox['y0']=300
        bbox['y0']=300
    if bbox['x0']>1100:
        bbox['x0']=1100
        bbox['x1']=1100
    # demo only shows the first point, but other points may also be available
    try:
        userhoverdata=hoverData["points"][0]['customdata']
    except:
        children=[]
        bbox=None
        return True, bbox,no_update, children
    title1 = f"{userhoverdata['Username']}:  "
    title2 = f"Start: {userhoverdata['Start']}   End: {userhoverdata['End']} "
    title3=f"Deadline: {userhoverdata['Deadline']}"
    title4=f"Average last two days duration:  {userhoverdata['Average2Days']}"
    title5=f"Maximum last two days duration:  {userhoverdata['Max2Days']}"
    children = [html.Div(children=[
            html.Strong(title1, style={'font-weight': 'bold'}),
            title2,html.P(title3),html.P(title4),html.P(title5),
            dcc.Graph(
                id='minichart',
                figure=make_minichart(userhoverdata),
                config={'displayModeBar': False},
                style={'margin-top': '-20px'}
            )],style={'background-color':userhoverdata['color']})
            
    ]
    tooltip_style = {'background-color':userhoverdata['color'],'position': 'absolute','top':'100'} 
    return True,bbox,tooltip_style, children


# This is to fetch the data again if page reloads
@app.callback(Output('mainapp','children'),
              Input('url', 'pathname'))
def display_page(pathname):
    global days,data,legend_data
    if pathname == '/':
        data,days,legend_data=process_data()
        return get_mainapp_container(data,days,legend_data)

# Run the app
if __name__ == '__main__':
    # yaxislabel()
    app.run_server(debug=True)
#Remember the day width and bar width should be choosen carefully,
#DAY_WIDTH value should not be less than number of users *(BAR_WIDTH+BAR_MARGIN)
#test for 2nd commit
BAR_WIDTH = 0.09
BAR_GAP=0.2
DAY_WIDTH=1
BAR_MARGIN=0.1
BAR_PERIMETER=1
DAY_MARGIN_COLOR='blue'
DEADLINE_MARKER_WIDTH=3
WIDTH_YMAJOR=0.6
WIDTH_YMINOR=0.3
GRAPH_HEIGHT=600
GRAPH_WIDTH=1500
# Random colors for users
colors_list = ["aquamarine","blueviolet","cadetblue","chartreuse",
"coral","chocolate","cyan","darkorange","fuchsia",
"green","greenyellow","limegreen","thistle","teal","tan",
"sienna","seagreen","salmon","saddlebrown","purple",
"peru","olive","grey","darkorchid","brown"]